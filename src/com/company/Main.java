package com.company;
import com.mysql.jdbc.Driver;
import java.sql.*;


class Emp {
    public String name;
    public String address;
    public int age;
    Emp(String name, String address, int age ) {
        this.name = name;
        this.address = address;
        this.age =age;
    }
}

public class Main {

    public static void main(String[] args) throws  Exception{
	// write your code here
        //Connection
        String url ="jdbc:mysql://localhost/";
        String username ="root";
        String password = "";
        String dbName = "employee";

        //queryInput
        boolean databaseExist = false;
        String qCreateDatabase = "CREATE database " + dbName;
        String qDeleteDatabase = "DROP database " + dbName;
        String qCreateTable = "CREATE TABLE EmployeeID " +
                "(id int not NULL AUTO_INCREMENT, " +
                " name VARCHAR(255), " +
                " address VARCHAR(255), " +
                " age int, " +
                " PRIMARY KEY ( id ))";
        String qShowTable = "SELECT * FROM EmployeeID";
        String qCreateNewID = "INSERT INTO EmployeeID (name, address, age) VALUES (?,?,?)";

        //NewUser
        Emp[] users;
        int size = 3;
        users = new Emp[size];
        users[0] = new Emp("Jake", "Jalan B no. 10", 29);
        users[1] = new Emp("John", "Jalan D no. 9", 35);
        users[2] = new Emp("Mary", "Jalan C no. 8", 32);

        Connection con = DriverManager.getConnection(url, username, password);

        //Checking whether the database exists
        ResultSet res = con.getMetaData().getCatalogs();
        while (res.next()) {
            databaseExist = false;
            String databaseName = res.getString(1).toLowerCase();
            if(databaseName.equals(dbName)){
                databaseExist = true;
                break;
            }
        }
        con.close();

        //Create Database
        if(!databaseExist) {
            //Creating Database and Table
            con = DriverManager.getConnection(url, username, password);
            Statement st = con.createStatement();
            st.execute(qCreateDatabase);
            st.close();
            con.close();

            //Connect to newly created database
            con = DriverManager.getConnection(url + dbName, username, password);
            st = con.createStatement();
            st.execute(qCreateTable);
            st.close();
            con.close();
        }

        //Connecting to newly created / existing database
        con = DriverManager.getConnection(url + dbName, username, password);
        PreparedStatement stm = con.prepareStatement(qCreateNewID);
        for(int i=0; i<users.length;i++){
            stm.setString(1, users[i].name);
            stm.setString(2, users[i].address);
            stm.setInt(3, users[i].age);
            stm.executeUpdate();
        }
        stm.close();

        //querying
        String data ="";
        Statement s = con.createStatement();
        ResultSet rs = s.executeQuery(qShowTable);
        while(rs.next()){
            data = rs.getInt(1) + " | " + rs.getString(2) + " | " +
                    rs.getString(3) + " | " + rs.getString(4);
            System.out.println(data);
        }
        s.close();
        con.close();

        // Drop Database
        con = DriverManager.getConnection(url, username, password);
        Statement st = con.createStatement();
        st.execute(qDeleteDatabase);
        st.close();
        con.close();

    }
}
